// Vérifie si l'URL de la page correspond au pattern

// Extrait le customerFileId de l'URL
const filmTitle = document.getElementsByTagName('h1');



// Crée le bouton
const button = document.createElement('button');
button.textContent = filmTitle[0].textContent + 'sur YGG Torrent';
button.style.position = 'fixed';
button.style.bottom = '10px';
button.style.left = '10px';
button.style.zIndex = '1000';
button.style.background = '#5ad9a4';
button.style.color = 'white';
button.style.fontWeight = '700';
button.style.padding = '20px';
button.style.borderRadius = '10em';
button.style.border = '3px solid #5ad9a4';

// Ajoute l'événement click au bouton pour rediriger vers la nouvelle URL
button.addEventListener('click', () => {
    //window.location.href = 'https://www3.yggtorrent.wtf/engine/search?name='+ filmTitle[0].textContent + '&do=search';
    window.open('https://www3.yggtorrent.wtf/engine/search?name=' + filmTitle[0].textContent + '&do=search', "blank");
});

// Ajoute le bouton à la page
document.body.appendChild(button);
